Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'home#index'
  resources :home, only:[:index]
  resources :registry, only:[:index, :create, :edit, :update, :show, :destroy] do
    collection do
      post 'search'
      get 'sort'
    end
    member do
      post 'publish'
      post 'unpublish'
    end
  end
end
