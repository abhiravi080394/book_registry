

#category seed
if Category.count == 0
  categories = ["Adventure", "Arts", "Biographies", "Economics", "Comics", "Computing", "Thriller", "Fantasy", "Personal Development"]
  categories.each do |category_name|
    Category.create(name: category_name)
  end
end

#For demo purpose 
#books seed
if Book.count == 0 && Category.count != 0
  books = [
  	        [ "Think and Grow Rich", "There are hundreds and thousands of successful people in the world who can vouch for the contents of this book", "Napoleon Hill", "8192910911", 69, 4, true], 
  	        [ "The Alchemist", "Paulo Coelho's enchanting novel has inspired a devoted following around the world.", "Paulo Coelho", "8172234988", 166, 8, true], 
  	        [ "The Theory of Everything", "He describes what a theory that can state the initiation of everything would encompass.", "Stephen Hawking", "8179925919", 109, 6, true], 
  	        [ "Inner Engineering: A Yogi’s Guide to Joy", "‘Inner Engineering is a fascinating read, rich with Sadhguru’s insights and his teachings.", "Sadhguru", "0143428845", 150, 9, true], 
  	        [ "Attitude Is Everything", "The power of attitude can change your destiny. ", "Jeff Keller", "9351772071", 100, 9, true], 
  	        [ "The Monk Who Sold His Ferrari", "The plot of this story revolves around Julian Mantle, a lawyer who has made his fortune and name in the profession", "Robin Sharma", "978-8179921623", 163, 9, false ]
  	      ]
  books.each do |book|
  	Book.create(book_name: book[0], book_description: book[1], author_name: book[2], book_isbn: book[3], price: book[4], category_id: book[5], publish_status: book[6])
  end
end
