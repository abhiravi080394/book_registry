class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books do |t|
      t.string :book_name
      t.text :book_description
      t.string :author_name
      t.string :book_isbn
      t.numeric :price, precision: 15, scale: 2
      t.integer :category_id
      t.boolean :publish_status

      t.timestamps
    end
    add_index :books, [:book_isbn], :unique => true
  end
end
