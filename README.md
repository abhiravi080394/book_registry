Project Specs:

Name: Book-Registry App

Rails version:
rails "5.2.1"

Ruby version:
ruby "2.5.1"

Database:
Postgres - "1.0.0"

Steps:
1) Extract zip
2) Open terminal within project directory
3) Run "bundle install" to install gems
4) Run "rails db:setup" to create the database
5) Run "rails db:migrate" to run migrations 
5) Run "rails db:seed" to seed demo books and categories 
6) Run rails server "rails s"

References:
1) Postgres installation: https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-16-04

2) Postgres issues: http://suite.opengeo.org/docs/latest/dataadmin/pgGettingStarted/firstconnect.html

3) Gitlab public repo: https://gitlab.com/abhiravi080394/book_registry


Project Includes:
1) Manage registry: list all books, CRUD operations, publish/unpublish functionality , search and sort.
2) Home: List all published books.

3) Pages: Home, Manage Registry, View book info , Edit book info, Add new book



Does not include: tests.
