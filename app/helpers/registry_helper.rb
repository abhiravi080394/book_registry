module RegistryHelper
  def isPublished(publish_status, book_id)
  	return publish_status ? "<a href='/registry/#{book_id}/unpublish' class='btn btn-outline-warning' data-method='post'>Unpublish</a>".html_safe : "<a href='/registry/#{book_id}/publish' class='btn btn-success' data-method='post'>Publish</a>".html_safe
  end
end
