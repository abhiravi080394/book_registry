class Book < ApplicationRecord
	belongs_to :category

	scope :published, -> { where(publish_status: true) }
	scope :with_category, -> { includes(:category).order("created_at DESC") }
	
	validates :book_name, :presence => {:message => "Book name can't be blank." }
	validates :book_description, :presence => {:message => "Book Description can't be blank." }
	validates :author_name, :presence => {:message => "Author name can't be blank." }
	validates :price, :presence => {:message => "Price can't be blank." }
	validates :book_isbn, 
      :presence => {:message => "ISBN can't be blank." },
      :uniqueness => {:message => "ISBN already exists."},
      :length => { :maximum => 100, :message => "Invalid ISBN"}
end
