class HomeController < ApplicationController

  def index
  	@books = Book.published
  end

end
