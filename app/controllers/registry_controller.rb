class RegistryController < ApplicationController
  
  def index
    @books = Book.with_category
    @categories = Category.all
    @new_book = Book.new
  end

  def create
    book = Book.new(book_params)
    if book.save
      redirect_to registry_index_path
    end
  end

  def show
    @book_info = Book.find(params[:id])
  end

  def edit
    @book = Book.find(params[:id])
    @categories = Category.all
  end

  def update
    @book = Book.find(params[:id])
    if @book.update(book_params)
      redirect_to registry_index_path
    end
  end

  def destroy
  	@book = Book.find(params[:id])
    if @book.destroy
      redirect_to registry_index_path
    end
  end

  def publish
    @book = Book.find(params[:id])
    if @book.update(publish_status: true)
      redirect_to registry_index_path
    end
  end

  def unpublish
    @book = Book.find(params[:id])
    if @book.update(publish_status: false)
      redirect_to registry_index_path
    end
  end

  def search
    @books = Book.where(search_query)
    respond_to do |format|
      format.html
      format.js
    end
  end

  def sort
    case params[:selected_value]
    when "book_name_asc"
      @books = Book.order("book_name ASC")
    when "book_name_desc"
      @books = Book.order("book_name DESC")
    when "author_name_asc"
      @books = Book.order("author_name ASC")
    when "author_name_desc"
      @books = Book.order("author_name DESC")
    when "price_low_high"
      @books = Book.order("price ASC")
    when "price_high_low"
      @books = Book.order("price DESC")
    end
  end
end

private

  def book_params
    params.require(:book).permit(:book_name, :book_description, :author_name, :book_isbn, :price, :publish_status, :category_id)
  end

  def search_query
    book_name = params[:search][:book_name]
    author_name = params[:search][:author_name]
    book_isbn = params[:search][:book_isbn]
    search_query = ""
    search_query += "book_name ILIKE '%"+book_name+"%'" if book_name.present?
    if book_name.present? && author_name.present?
      search_query += "AND author_name ILIKE '%"+author_name+"%'"
    elsif author_name.present?
      search_query += "author_name ILIKE '%"+author_name+"%'"
    end
    if (book_name.present? || author_name.present?) && book_isbn.present?
      search_query += "AND book_isbn ILIKE '"+book_isbn+"'"
    elsif book_isbn.present?
      search_query += "book_isbn ILIKE '"+book_isbn+"'"
    end
    search_query
  end